#! /usr/local/bin/php
<?php
/*
author: vizcreations
explain: self executable PHP script to migrate database changes, do update to your system
copyright: GPL

copyright (C) 2009
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


*/
define( "__MAGIXQUOTE", get_magic_quotes_gpc() ); // I just defined the server's behaviour for magic quote
define( "__ROOT", realpath(dirname( __FILE__ )) );
define( "__DS", DIRECTORY_SEPARATOR );
define( "__EXEC", true ); // Should we execute?
define( "__PHP", "mod" );
define( "__bin", "/usr/local/bin/php");

$exec = true;
$all = true;
$global = false;
$http = '_GET'; // The default http method
$httpvars = $_GET;
$argc = 0;
$argv = isset($_SERVER['argv'])?$_SERVER['argv']:array(0=>$_SERVER['PHP_SELF']); // The command line arguments
$schema = array(0);
$schema['curq'] = isset($httpvars['curq']) ? $httpvars['curq'] : 'all'; // The key you want to execute
$schema['driver'] = 'mysqli';
$driver = $schema['driver'];
$logsdir = __ROOT.__DS."logs"; // Change to your logs directory and make it world-writable
$tmpdir = __ROOT.__DS."tmp"; // Change to your temp directory and make it world-writable
$config = "config.php"; // you're website configuration to use global database credentials

// Your driver/extension
$mysql = array();
$mysql['core']['create'] = false;
$mysql['core']['schema'] = 'travelninja';
$mysql['core']['options'] = array( "", "", "", "", "", "" );
$err = '';

$conn = null;

if(!file_exists(__ROOT.__DS.$config)) {
	log_err( $config.": not found!" );
	exit(2);
}
require_once( __ROOT.__DS.$config ); // Link to your config file

// Function that logs the errors encountered throughout the execution of this script
function log_err($err='') {
	global $logsdir;
	if(!empty($err) 
		&& (is_dir($logsdir)&&is_writable($logsdir))
		) {
		$fp = fopen($logsdir.__DS."merror_log", "a+");
		if($fp) {
			fwrite($fp, $err.PHP_EOL);
			fclose($fp);
		}
	} else {
		printf( "Directory %s isn't writable!!!\n", $logsdir );
	}
}

/**
* Function that logs the current migration script execution to file
*/
function log_file($msg='') {
	global $logsdir;
	$date = getdate();
	$yr = $date['year'];
	$mon = $date['mon'];
	$day = $date['mday'];
	$wday = $date['wday'];
	$hour = $date['hours'];
	$min = $date['minutes'];
	$str = "(Script run on $wday $mon $day, $yr at $hour:$min)".PHP_EOL;
	if(true 
		&& (is_dir($logsdir)&&is_writable($logsdir))
		) {
		$fp = fopen($logsdir.__DS."migrate_log", "a+");
		if($fp) {
			if(!empty($msg)) $str .= " : ".$msg;
			fwrite($fp, $str.PHP_EOL);
			fclose($fp);
		}
	} //else log_err( "Couldn't write migrate error log!" );
	else printf( "Directory %s isn't writable!!!\n", $logsdir );
}

function connect_db($db) {
	$link = mysqli_connect($db['host'], $db['user'], $db['pass']);
	if(!$link) {
		log_err(mysqli_error($link));
		return false;
	}
	if(!mysqli_select_db($link, $db['schema'])) {
		log_err("Couldn't select database!");
		return false;
	}
	return $link;
}

function valid($q=null, $link=null) { // Blueprint that checks the query structure
	if( !__MAGIXQUOTE ) {
		$q = mysqli_real_escape_string($link, $q);
	}
	return true;
}

function execute($q=null, $link=null, $num=0) {
	printf( "%s\t", "Executing query($num): ..." );
	$res = null;
	if($q != null && $link != null) {
		$res = mysqli_query($link, $q);
	}
	sleep(1);
	if(!$res) {
		printf( "%s", "Error!!!" );
		printf( " << Check logs >>".PHP_EOL );
		log_err( mysqli_error($link) );
	} // Just log and do not return or exit
	else printf( "%s\n", "Success...");
	sleep(3);
}

// List of queries that you want to execute
$sql1 = "";
$sql2 = "";
$sql3 = "";
$sql4 = "";
$sql5 = "";
$sql6 = "";
$sql7 = "";
$sql8 = "";
$sql9 = "";
$sql10 = "";
$sql11 = "";


/**
* Change/Append your keys which have no effect to executing
*
*/
$sqlarray = array( 
			"sql1" => $sql1, // Rename key to the purpose of query
			"sql2" => $sql2,
			"sql3" => $sql3,
			"sql4" => $sql4,
			"sql5" => $sql5,
			"sql6" => $sql6,
			"sql7" => $sql7,
			"sql8" => $sql8,
			"sql9" => $sql9,
			"sql10" => $sql10,
			"sql11" => $sql11
		);


if(__EXEC == true) {
	date_default_timezone_set( 'UTC' ); // TimeZone 
	log_file(); // Logging when the script executed..
	$link = connect_db($dbserver); // an array of keys representing database creditial name=value pairs
	$key="null";
	if($link) {
		$num = 1;
		$argc = count($argv);
		if($argc>1) $httpvars['curq'] = "command";
		switch($schema['curq']) {
			case 'tip':
				log_file( "Tip query selected" );
				break; // Add you cases to run each query
			case 'all':
				if(isset($sqlarray) && is_array($sqlarray)) {
					foreach($sqlarray as $q) {
						if(!empty($q)) {
							if(valid($q, $link)) {
								execute($q, $link, $num);
								++$num;
							}
							else log_err( $q.": is invalid" );
						} else log_err( $q.": is empty" );
					}
				}
				break;
			default: // Invoke command line arguments
				$num=1;
				if($argc<2) { // Some key was defined..
					for($i=1;$i<$argc;$i++) {
						$key=$argv[$i];
						if(array_key_exists($sqlarray,$key)) {
							$q = $sqlarray[$key];
							if(!empty($q))
								if(valid($q,$link)) {
									execute($q,$link,$num); ++$num;
								} else log_err( $q.": is invalid");
							else log_err( $q.": is empty" );
						} else log_err( $q.": is invalid" );
					}
				} else if($argc>2) { // We have a range declared..
					/** Simply execute blindly using keys */

					for($i=$argv[1];$i<$argv[2]+1;$i++) {
						$key=$i;
						if(array_key_exists($sqlarray,$key)) {
							$q = $sqlarray[$key];
							if(!empty($q))
								if(valid($q,$link)) {
									execute($q,$link,$num); ++$num;
								} else log_err($q.": is invalid");
							else log_err($q.": is empty");
						} else log_err( $q.": is invalid" );
					}
				}
				break;
		}
		mysqli_close($link);
	} else log_err( "MYSQL identifier error" );
} else log_err( "EXEC constant not defined" );
